package factorys;

import abstracts.Calentador;
import abstracts.Lampara;
import herramientas.LamparaLuz;
import herramientas.CalentadorLuz;

public class FabricaHerramientasLuz implements FabricaHerramientas {

    @Override
    public Lampara createLampara(String tamano, String pilas, int nivel){

        return new LamparaLuz(tamano,pilas,nivel);
    }

    @Override
    public Calentador createCalentador(String modelo, String capacidad, int tubos){

        return new CalentadorLuz(modelo, capacidad, tubos);
    }





}
